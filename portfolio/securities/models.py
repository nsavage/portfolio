from django.db import models

# Create your models here.

class APIKey(models.Model):
    name = models.CharField(max_length=100, blank=True)
    apikey = models.CharField(max_length=100)

class Security(models.Model):
    symbol = models.CharField(max_length=100)
    name = models.CharField(max_length=100)
    description = models.TextField()

    exchange = models.CharField(max_length=100)
    year_end = models.CharField(max_length=100)
    country = models.CharField(max_length=100)

    asset_type = models.CharField(max_length=100)
    sector = models.CharField(max_length=100)
    industry = models.CharField(max_length=100)

    currency = models.CharField(max_length=3)
