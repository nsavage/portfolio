from django.contrib import admin

from portfolio.securities import models

# Register your models here.

@admin.register(models.APIKey)
class APIKeyAdmin(admin.ModelAdmin):
    list_display = ("name", "apikey")

@admin.register(models.Security)
class SecurityAdmin(admin.ModelAdmin):
    list_display = ("symbol", "name",)
