from django.core.management.base import BaseCommand

url = "https://www.alphavantage.co/query?function=OVERVIEW&symbol="

class Command(BaseCommand):
    
    def add_arguments(self, parser: "CommandParser") -> None:
        parser.add_argument(
            "--symbol",
            dest="symbol",
            action="store",
            required=True,
        )
    
    def handle(self, *args, **options):
        symbol = options["symbol"]

        
        pass
