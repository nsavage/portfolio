from django.core.management.base import BaseCommand

from portfolio.securities.models import APIKey

class Command(BaseCommand):
    
    def add_arguments(self, parser: "CommandParser") -> None:
        parser.add_argument(
            "--key",
            dest="key",
            action="store",
            required=True,
        )
    
    def handle(self, *args, **options):
        key = options["key"]

        key_obj, created = APIKey.objects.get_or_create(apikey=key)
        if created:
            print("Added new API Key")
        else:
            print("Key already exists, skipping import")
